/**
 * Express configuration
 */

'use strict';

var express = require('express');
// var favicon = require('static-favicon');
var morgan = require('morgan');
//var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var cors = require('cors');

var config = require('./singleenv');

var passport = require('passport');
// only fired if body-parser give error
function multipartBypassError(err, req, res, next) {
  console.log("this will be only trrigred if request type not matched with exprees-server has set");
  //console.log(next);
  //console.log(req.url)
  if (err) {
    console.error(err.stack);

    if (req.url.indexOf("customcollections_file") > -1) {
      console.log("it is multipart reuest for file upload .. do not throw err")
      next(null)

    }
    else {
      console.log("it is not multipart file upload reuest so content-type must be 'appilcation/json' as full api is desgined to support only 'json' request. :: not :: 'x-www-form-urlencoded'")

      //console.log(req.url.indexOf("customcollections_file"))

      if (req.xhr) {
        console.log("xhr reuest found")
        res.json(500, {
    
          "status_code": 0,
    
          "http_status": 500,
    
          "error_code": "api support only 'content-type' : 'json' request and multipart request for only file upload",
    
          "error_log": JSON.stringify(err),
    
          "extra_msg": { "ajaxcall": true }
    
        })
      }
      else {
        console.log("xhr reuest not found")
        res.json(500, {
    
          "status_code": 0,
    
          "http_status": 500,
    
          "error_code": "api support only 'content-type' : 'json' request and multipart request for only file upload ",
    
          "error_log": JSON.stringify(err),
    
          "extra_msg": { "ajaxcall": false }
    
        })
    
      }
    }

  }
  else {
    console.log("no errors ... ")

  }
}

module.exports = function (app) {
  var env = app.get('env');





  app.set('views', config.root + '/server/views');
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  //app.use(compression());

  //app.use(bodyParser.urlencoded({ extended: false }));

  app.use(bodyParser.json({ type: 'application/json' }));

  app.use(cookieParser());
  app.use(passport.initialize());

  if ('production' === env) {
    console.log("express env " + env + " config.root " + config.root)
    // app.use(favicon(path.join(config.root, 'public', 'favicon.ico')));
    app.use(express.static(path.join(config.root, 'public')));
    app.set('appPath', config.root + '/public');
    app.use(morgan('dev'));

  }

  if ('development' === env || 'test' === env) {

    console.log("express env " + env + " config.root " + config.root)

    app.use(require('connect-livereload')());
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'client')));
    app.set('appPath', 'client');
    app.use(morgan('dev'));



    var corsOptionsDelegate = function (req, callback) {
      var corsOptions;

      corsOptions = { origin: req.header('Origin') } // reflect (enable) the requested origin in the CORS response

      corsOptions.methods = ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'];
      corsOptions.allowedHeaders = ['origin', 'accept', ' content-type', 'Authorization', 'Accept', 'Accept-Encoding', 'Accept-Language', 'Connection', 'Content-Length', 'Content-Type', 'Host', 'Origin', 'Referer', 'User-Agent', 'X-DevTools-Emulate-Network-Conditions-Client-Id', 'X-Requested-With', 'X-HTTP-Method-Override', 'mobile_device_id']
      corsOptions.credentials = true;
      corsOptions.preflightContinue = true;
      callback(null, corsOptions) // callback expects two parameters: error and options
    }

    app.options('*', cors(corsOptionsDelegate));
    app.use(cors(corsOptionsDelegate));

    app.use(multipartBypassError);
    
  }

};

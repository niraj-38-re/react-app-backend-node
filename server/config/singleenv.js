'use strict';

var path = require('path');

var development = {
    env: process.env.NODE_ENV,

    // Root path of server
    root: path.normalize(__dirname + '/../..'),

    // Server port
    // port: process.env.PORT || 9000,
    port: process.env.PORT || 9000,

    
    // Should we populate the DB with sample data?
    seedDB: true,

    // Secret for session, you will want to change this and make it an environment variable
    secrets: {
        session: 'fullstack2-secret'
    },

    jwt_redis_secrets: {
        session: 'jwt_redis_secrets_key-value'

    },   

    // MongoDB connection options
    mongo: {
        uri: process.env.MONGO_URL || 'mongodb://localhost/fullstack2-dev',
        options: {
            db: {
                safe: true
            }
        }
    },

    facebook: {
        clientID:     process.env.FACEBOOK_ID || '',
        clientSecret: process.env.FACEBOOK_SECRET || '',
        callbackURL:  process.env.FACEBOOK_DOMAIN  || process.env.DOMAIN + '/auth/facebook/callback'
    },

    twitter: {
        clientID:     process.env.TWITTER_ID || 'id',
        clientSecret: process.env.TWITTER_SECRET || 'secret',
        callbackURL:  process.env.TWITTER_DOMAIN || process.env.DOMAIN +  '/auth/twitter/callback'
    },

    google: {
        clientID:     process.env.GOOGLE_ID || 'id',
        clientSecret: process.env.GOOGLE_SECRET || 'secret',
        callbackURL:  process.env.GOOGLE_DOMAIN || process.env.DOMAIN + '/auth/google/callback'
    },

    
    aws_s3: {
        aws_s3_bucket_file_limit: process.env.AWS_S3_BUCKET_FILE_LIMIT || 20,

        aws_s3_bucket_name: process.env.AWS_S3_BUCKET_NAME || '',

        aws_s3_bucket_accesskeyid: process.env.AWS_S3_BUCKET_KEY || '',

        aws_s3_bucket_secretaccesskey: process.env.AWS_S3_BUCKET_SECRET || ''

    }

};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = development

'use strict';

var _ = require('lodash');
var Requestlog = require('./requestlog.model');

// Get list of requestlogs
exports.index = function(req, res) {
  Requestlog.find(function (err, requestlogs) {
    if(err) { return handleError(res, err); }
    return res.json(200, requestlogs);
  });
};

// Get a single requestlog
exports.show = function(req, res) {
  Requestlog.findById(req.params.id, function (err, requestlog) {
    if(err) { return handleError(res, err); }
    if(!requestlog) { return res.send(404); }
    return res.json(requestlog);
  });
};

// Creates a new requestlog in the DB.
exports.create = function(req, res) {
  Requestlog.create(req.body, function(err, requestlog) {
    if(err) { return handleError(res, err); }
    return res.json(201, requestlog);
  });
};

// Updates an existing requestlog in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Requestlog.findById(req.params.id, function (err, requestlog) {
    if (err) { return handleError(res, err); }
    if(!requestlog) { return res.send(404); }
    var updated = _.merge(requestlog, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, requestlog);
    });
  });
};

// Deletes a requestlog from the DB.
exports.destroy = function(req, res) {
  Requestlog.findById(req.params.id, function (err, requestlog) {
    if(err) { return handleError(res, err); }
    if(!requestlog) { return res.send(404); }
    requestlog.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
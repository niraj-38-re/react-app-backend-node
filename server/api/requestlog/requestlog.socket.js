/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Requestlog = require('./requestlog.model');

exports.register = function(socket) {
  Requestlog.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Requestlog.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('requestlog:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('requestlog:remove', doc);
}
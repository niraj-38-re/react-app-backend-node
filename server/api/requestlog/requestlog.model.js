'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var RequestlogSchema = new Schema({

  name	: { "type" : String , "required": true   },
  
  imageUrl	: 	{ type: String },

  price	: 	{ type: String },
  // price	: 	{ type: Number },
  qty	: 	{ type: String },

  createdAt:{"type":Date,"required":true, "default": Date.now},

  updatedAt:{"type":Date,"required":true, "default": Date.now}
				   

});

module.exports = mongoose.model('Requestlog', RequestlogSchema);